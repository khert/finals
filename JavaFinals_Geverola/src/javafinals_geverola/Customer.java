/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafinals_geverola;

/**
 *
 * @author pn
 */
public class Customer {
    private int id;
    private String fName;
    private String lName;
    private String status;
    private String company;
    private float salary;
    private int plan_id;
    private int cashier_id;

    public Customer() {
    }

    public Customer(int id, String fName, String lName, String status, String company, float salary, int plan_id, int cashier_id) {
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.status = status;
        this.company = company;
        this.salary = salary;
        this.plan_id = plan_id;
        this.cashier_id = cashier_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public int getCashier_id() {
        return cashier_id;
    }

    public void setCashier_id(int cashier_id) {
        this.cashier_id = cashier_id;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", fName=" + fName + ", lName=" + lName + ", status=" + status + ", company=" + company + ", salary=" + salary + ", plan_id=" + plan_id + ", cashier_id=" + cashier_id + '}';
    }
    
    
    
    
}
