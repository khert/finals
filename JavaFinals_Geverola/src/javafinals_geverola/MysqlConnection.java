/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafinals_geverola;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pn
 */
public class MysqlConnection {
    private String driver;
    private String host;
    private String dbName;
    private String user;
    private String password;

    public MysqlConnection() {
        initiateDriver();
    }

    public MysqlConnection(String driver, String host, String dbName, String user, String password) {
        this.driver = driver;
        this.host = host;
        this.dbName = dbName;
        this.user = user;
        this.password = password;
        initiateDriver();
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    private void initiateDriver(){
        try {
            
            Class.forName(getDriver());    
            
        } catch (ClassNotFoundException e) {
            System.err.println("Driver not found !!!!");
            System.out.println(e);
        }
    }
    private Connection initiateConnection(){
        Connection con = null;
        try {
            con = DriverManager.getConnection(getHost()+"/"+getDbName(),getUser(),getPassword());
        } catch (SQLException e) {
        }
        return con;
    }
    public ArrayList<Plan> getPlan(){
        ArrayList<Plan> list = new ArrayList<>();
        
        try {
            PreparedStatement ps = initiateConnection().prepareStatement("SELECT * FROM plan");
            ResultSet rs =  ps.executeQuery();
            
            while(rs.next()){
                Plan temp = new Plan();
                temp.setId(rs.getInt(1));
                temp.setTitle(rs.getString(2));
                temp.setBrand(rs.getString(3));
                temp.setData(rs.getString(4));
                temp.setInstallment(rs.getString(5));
                
                list.add(temp);
            }
            
        } catch (SQLException ex) {
            System.out.println("ERROR GETTING PLAN DATA ");
            Logger.getLogger(MysqlConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return list;
    }
    public Cashier getCashier(){
        Cashier cash = new Cashier();
         try {
            PreparedStatement ps = initiateConnection().prepareStatement("SELECT * FROM cashier WHERE cashier_id = ?");
            ps.setInt(1, 1);
            ResultSet rs =  ps.executeQuery();
            
            while(rs.next()){
               cash.setId(rs.getInt(1));
               cash.setfName(rs.getString(2));
               cash.setlName(rs.getString(3));
            }
            
        } catch (SQLException ex) {
            System.out.println("ERROR GETTING PLAN DATA ");
            Logger.getLogger(MysqlConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return  cash;
        
    }

    public boolean buy(Customer temp, Cashier cashier, int plan) {
        boolean bool = false;
          try {
            String sql = "INSERT INTO `customer`( `fname`, `lastname`, `status`, `company`, `salary`, `fk_plan_id`, `fk_cashier_id`) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement ps = initiateConnection().prepareStatement(sql);
            ps.setString(1, temp.getfName());
            ps.setString(2,temp.getlName());
            ps.setString(3, temp.getStatus());
            ps.setString(4,temp.getCompany());
            ps.setFloat(5,temp.getSalary());
            ps.setInt(6, plan);
            ps.setInt(7, cashier.getId());
            
            int i  = ps.executeUpdate();
            if(i == 1){
                bool = true;
            }
            
           
            
        } catch (SQLException ex) {
            System.out.println("ERROR GETTING PLAN DATA ");
            Logger.getLogger(MysqlConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return bool;
    }
    
}
