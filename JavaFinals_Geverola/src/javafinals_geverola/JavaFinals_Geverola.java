/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafinals_geverola;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pn
 */
public class JavaFinals_Geverola {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        MysqlConnection conn = new MysqlConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306", "finals", "root", "");

        Cashier cashier = conn.getCashier();
        ArrayList<Plan> plans = conn.getPlan();
        Scanner scan = new Scanner(System.in);

     
        int num = 1;
        do {
            Customer temp = new Customer();
            int plan = 0;
            boolean val = true;
            
            System.out.println("WELCOME TO GLOBE MOBILE PLAN\nChoose Plan :\n   1 = Basic Plan\n   2 = Silver Plan\n   3 = Platinum Plan\n   4 = Exit");
          
            while (val) {
                try {

                    System.out.print("\nEnter Choice: ");
                    plan = new Scanner(System.in).nextInt();
                    if(!(plan > 0 && plan < 5)){
                        System.err.println("PLEASE PICK A NUMBER FROM THE Choose Plan");
                    }else{
                        val = false;
                    }
                    
                } catch (Exception e) {
                    System.err.println("PLEASE INPUT A NUMBER");
                }
            }
            if(plan < 4){
                System.out.print("\nInput firstname :");
                String fname = scan.next();
                temp.setfName(fname);
                System.out.print("Input lastname  :");
                String lname = scan.next();
                temp.setlName(lname);
                System.out.print("Input status    :");
                String status = scan.next();
                temp.setStatus(status);
                System.out.print("Input company   :");
                String company = scan.next();
                temp.setCompany(company);
                val = true;
                while (val) {
                    try {
                        System.out.print("Input salary    :");
                        float salary = new Scanner(System.in).nextFloat();
                        temp.setSalary(salary);
                        val = false;
                    } catch (Exception e) {
                        System.err.println("PLEASE INPUT A NUMBER");
                    }
                }
                val= true;
                int x = 0;
                while (val) {
                    try {
                        System.out.print("\nContinue Purchase:\n1 = Yes\n2 = No\n");
                        System.out.print("\nENTER CHOICE: ");
                        x =  new Scanner(System.in).nextInt();
                        if(x > 0 && x < 3){
                            val = false;
                        }else{
                            
                            System.err.println("PLEASE CHOOSE 1 OR 2");
                        }
                    } catch (Exception e) {
                        System.err.println("PLEASE INPUT A NUMBER");
                    }
                }
                if(x == 1){
                    boolean b = conn.buy(temp,cashier,plan);
                    if(b){
                        System.out.println("\n"+temp.getfName()+" purchase "+ getPlan(plan)+"\n");
                        System.out.println("PURCHASE SUCCESS  ...");
                    }
                }
                
                
                System.out.println("Do you want to continue?\n1 = Yes\n2 = No");
                num = scan.nextInt();

            }else{
                num = plan;
            }
            
            

           

           

        } while (num == 1);

    }
    public static String getPlan(int plan){
        String temp = "";
        if(plan == 1){
            temp  = "Basic Plan";
        }else if(plan == 2){
            temp  = "Silver Plan";
        }else{
            temp  = "Platinum Plan";
        }
        
        
        return temp;
    }

}
