/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafinals_geverola;

/**
 *
 * @author pn
 */
public class Plan {
    private int id;
    private String title;
    private String brand;
    private String data;
    private String installment;

    public Plan() {
    }

    public Plan(int id, String title, String brand, String data, String installment) {
        this.id = id;
        this.title = title;
        this.brand = brand;
        this.data = data;
        this.installment = installment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    @Override
    public String toString() {
        return "Plan{" + "id=" + id + ", title=" + title + ", brand=" + brand + ", data=" + data + ", installment=" + installment + '}';
    }

    
    
    
}
